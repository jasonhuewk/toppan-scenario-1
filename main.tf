#
# Ref: https://github.com/jdluther2020/terraform-create-eks-k8s-cluster.git
#
# Create EKS Cluster - Entry Point
#

locals {
  namespace = "${var.namespace}"
  project_name = "${var.project_name}"
}


module "module_network" {
  source      = "./module-network"
  namespace = local.namespace
  project_name = local.project_name
}

module "module_s3bucket" {
  source      = "./module-s3bucket"
  namespace = local.namespace
  project_name = local.project_name
}

module "module_instance" {
  source      = "./module-instance"
  namespace = local.namespace
  project_name = local.project_name
  aws_lb_target_group = module.module_network.aws_lb_target_group
  subnet = module.module_network.aws_subnet_private_a
  config = module.module_s3bucket.s3_bucket_access_config
}

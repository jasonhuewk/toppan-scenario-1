#
# Ref: https://github.com/jdluther2020/terraform-create-eks-k8s-cluster.git
#
# 
#

variable "aws_region" {
  description = "AWS region"
  type        = string
  default     = "ap-southeast-1"
}

variable "namespace" {
  type = string
  default     = "toppan"
}

variable "project_name" {
  type = string
  default     = "scenario-1"
}
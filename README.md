# Completed part
- this terraform to bring up infra, instances and web page base on requirement. Web page served from nginx downloaded from S3 bucket

# Uncompleted part
- aws_access_key_id should not be hard code in cloud_config.yaml 
- CI part should able to validate terraform
- tfstate file should be backend in s3 bucket with encryption, versioning, locking feature, private and sharable between team member
- Convert text page to uppercase
- Instance refresh from gitlab to aws instance template not yet completed

# Recommendate part
- Should use SSL between different component
- The web page storing counter in client browser, counter not persist and shareable between user and browser. Store counter to persistence layer so that it can be shareable 
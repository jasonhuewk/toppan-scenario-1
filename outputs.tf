#
# Ref: https://github.com/jdluther2020/terraform-create-eks-k8s-cluster.git
#
#

output "region" {
  description = "AWS region"
  value       = var.aws_region
}

output "public_subnet" {
  value = module.module_network.subnet
}

output "alb" {
  value = module.module_network.alb.lb.dns_name
}
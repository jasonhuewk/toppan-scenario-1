#data "aws_availability_zones" "all" {}

resource "aws_security_group" "instance" {
  name = "${var.namespace}-${var.project_name}-sg"


  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 22
    to_port = 22
    protocol = "tcp"
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

data "cloudinit_config" "httpserver" {
  gzip          = true
  base64_encode = true
  part {
    content_type = "text/cloud-config"
    content      = templatefile("${path.module}/cloud_config.yaml", var.config )
  }
}

resource "aws_key_pair" "ssh" {
  public_key = var.ssh_key
  key_name   = "ssh"
}

resource "aws_launch_template" "instance" {
  name_prefix   = "webapp-instance"
  image_id      = "ami-082b1f4237bd816a1"
  instance_type = "t2.micro"
  key_name = aws_key_pair.ssh.key_name
  vpc_security_group_ids = [aws_security_group.instance.id]
  user_data = data.cloudinit_config.httpserver.rendered
}
/*
data "aws_vpc" "default" {
  default = true
}

data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }
}
*/

resource "aws_autoscaling_group" "fleet" {
  name                = "${var.namespace}-${var.project_name}-asg"
  #count             = length(data.aws_availability_zones.all.names)
  vpc_zone_identifier = [var.subnet]
  # use default vpc subnet to enable internet routing
  #vpc_zone_identifier = data.aws_subnets.default.ids
  target_group_arns = [var.aws_lb_target_group]
  desired_capacity   = 1
  max_size           = 1
  min_size           = 1

  launch_template {
    id      = aws_launch_template.instance.id
    version = "$Latest"
  }
}
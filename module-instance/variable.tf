variable "aws_lb_target_group" {
  type = string
}

variable "subnet" {
  type = string
}

variable "namespace" {
  type = string
}

variable "project_name" {
  type = string
}

variable "config" {
  type = object( {
    s3_access_role     = string
    bucket_name = string
  })
}

variable "ssh_key" {
  type = string
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDTxGVy50c7ryg+eXdZcFm01bIWRzKrLH2GQ4MC7JmfwUoV9Tvb3jXef4QdqthM6wKqsrfEB2hTRY1sv/8K0Qx2zW/oQ3mp7RB7IfkdIK7FVdP4uClJGvbXhTCQN0L7isiEyV9CY3Fe+VRfccu+YsBPcU7+NoITst81yestcrtUPQiVtIWApcv+M0KAarewc/xj3WC6bap9lQLswzwfk2FUAg5PNlICrVNmrX3Yg8tSTp58qp/n1YesXNi4RadKPfyFcn+TqJ9H4X78XZnhuHlLDFC1ILpxxZyx/PXWfCFdHgnI0p2K93ohFA4XIh+7qljNOniVruKrGGrNXi+Bb9C29J+RepK/LAktTv/eUUtjaYKYcWgyY1PfDClwQCPKHfpn9E6xqUMXfgsZOQsUitJdiZc5hH8fkfea/ScXxRLsH5R0OjUCQ5wrRHzLqrBKYeQ1xZOFIdpZ5x1dyBPPpVPGyLCGBHTvo8k4/V4vqPvgc7cZpVFXrZY95gIJ02Gn4s0= jason@jason-MINIPC-PN50"
  
}

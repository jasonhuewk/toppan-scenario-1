locals {
  subnet_ips = {
    public_a  = "172.31.48.0/24"
    public_b  = "172.31.49.0/24"
    private_a = "172.31.50.0/24"
    private_b = "172.31.51.0/24"
  }
  internet_gateway_id = data.aws_internet_gateway.default.id
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }
}

resource "aws_default_vpc" "default" {}

data "aws_region" "current" {
}

resource "aws_security_group" "load_balancer" {
  name        = "${var.namespace}-${var.project_name}-alb"
  description = "Allow traffic to loadbalancer"
  vpc_id      = aws_default_vpc.default.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
    self        = false
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_lb" "load_balancer" {
  name               = "${var.namespace}-${var.project_name}-alb"
  load_balancer_type = "application"
  internal           = false
  subnets            = data.aws_subnets.default.ids
  security_groups = [aws_security_group.load_balancer.id]
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.load_balancer.arn
  port              = 80
  protocol          = "HTTP"

  # By default, return a simple 404 page
  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404: page not found"
      status_code  = 404
    }
  }
}

resource "aws_lb_listener_rule" "asg" {
  listener_arn = aws_lb_listener.http.arn
  priority     = 100

  condition {
    path_pattern {
      values = ["*"]
    }
  }

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.asg.arn
  }
}



resource "aws_lb_target_group" "asg" {

  name = "${var.namespace}-${var.project_name}-alb"

  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_default_vpc.default.id

  health_check {
    path                = "/"
    protocol            = "HTTP"
    matcher             = "200"
    interval            = 15
    timeout             = 3
    healthy_threshold   = 2
    unhealthy_threshold = 5
  }
}

# internet gateway and NAT 
/*

resource "aws_internet_gateway" "nat_gateway" {
  vpc_id = data.aws_vpc.default.id
}
*/

resource "aws_subnet" "nat_gateway" {
  vpc_id                  = aws_default_vpc.default.id
  cidr_block              = local.subnet_ips.public_a
  availability_zone       = "${data.aws_region.current.name}a"
  map_public_ip_on_launch = true
  tags                    = {
    Name = "public_a"
  }
}


data "aws_internet_gateway" "default" {
  filter {
    name   = "attachment.vpc-id"
    values = [data.aws_vpc.default.id]
  }
}


resource "aws_route_table" "nat_gateway" {
  vpc_id = data.aws_vpc.default.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = local.internet_gateway_id
  }
}


resource "aws_route_table_association" "nat_gateway" {
  subnet_id = aws_subnet.nat_gateway.id
  route_table_id = aws_route_table.nat_gateway.id
}

resource "aws_eip" "ip" {
  vpc = true
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.ip.id
  subnet_id     = aws_subnet.nat_gateway.id
}

resource "aws_subnet" "private_a" {
  vpc_id                  = aws_default_vpc.default.id
  availability_zone       = "${data.aws_region.current.name}a"
  cidr_block              = local.subnet_ips.private_a
  map_public_ip_on_launch = false
  tags                    = {
    Name = "private_a"
  }
}

resource "aws_route_table" "private" {
  vpc_id = aws_default_vpc.default.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gateway.id
  }
}


resource "aws_route_table_association" "private_to_private" {
  route_table_id = aws_route_table.private.id
  subnet_id      = aws_subnet.private_a.id
}



output "vpc" {
  value = aws_default_vpc.default
}
#value = [for each in aws_subnet.map : if each. == true]
output "subnet" {
  value = {
    private_a = aws_subnet.private_a
  }
}

output "alb" {
  value = {
    lb = aws_lb.load_balancer
    sg = aws_security_group.load_balancer
  }
}

output "aws_lb_target_group" {
  value = aws_lb_target_group.asg.arn
}

output "aws_subnet_private_a" {
  value = aws_subnet.private_a.id
}

/*
output "nat_gateway_ip" {
  value = aws_eip.nat_gateway.public_ip
}
*/
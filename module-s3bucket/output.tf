output "s3_bucket_access_config" {
  value = {
    s3_access_role = aws_iam_role.s3_access_role.arn
    bucket_name = aws_s3_bucket.private_bucket.id
  }
}

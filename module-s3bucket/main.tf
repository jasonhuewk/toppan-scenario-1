resource "aws_iam_role" "s3_access_role" {
  name = "${var.namespace}-${var.project_name}-s3-access-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_s3_bucket" "private_bucket" {
  bucket = "${var.namespace}-${var.project_name}-bucket"
}

resource "aws_iam_role_policy_attachment" "s3_access_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
  role = aws_iam_role.s3_access_role.name
}

resource "aws_s3_bucket_policy" "s3_access_bucket_policy" {
  bucket = aws_s3_bucket.private_bucket.id
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:GetObject",
          "s3:PutObject"
        ]
        Effect = "Allow"
        Resource = "${aws_s3_bucket.private_bucket.arn}/*"
        Principal = {
          AWS = aws_iam_role.s3_access_role.arn
        }
      }
    ]
  })
}

resource "aws_s3_bucket_object" "index_html" {
  bucket = aws_s3_bucket.private_bucket.id
  key    = "index.html"
  source = "${path.module}/index.html"

  etag   = "${filemd5("${path.module}/index.html")}"

  # Grant permissions to the IAM role
  server_side_encryption = "AES256"
  acl                    = "private"
  content_type           = "text/plain"

}